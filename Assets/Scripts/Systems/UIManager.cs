using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
   [Title("GENERAL PROPERTIES")]
   [SerializeField] private ScriptableSettings settingsFile;
   [SerializeField] private GameObject closeDockButton;
   [SerializeField] private GameObject openDockButton;
   [SerializeField] private GameObject closeSubDockButton;
   [SerializeField] private CanvasGroup helpPanel;
   [SerializeField] private GameObject helpButton;
   
   [Space(20)][Title("DOCK PROPERTIES")]
   [SerializeField] private Transform dockObject;
   [SerializeField] private Transform dockOpenAnchor;
   [SerializeField] private Transform dockClosedAnchor;

   [Space(20)] [Title("SUB DOCK")]
   [SerializeField] private Transform subDockObject;
   [SerializeField] private Transform subDockOpenAnchor;
   [SerializeField] private Transform subDockClosedAnchor;
   [SerializeField] private TextMeshProUGUI startPointCoordinatesText;
   [SerializeField] private TextMeshProUGUI endPointCoordinatesText;
   [SerializeField] private TextMeshProUGUI distanceText;
   [SerializeField] private TextMeshProUGUI currentTileCoordinatesText;
   [SerializeField] private TextMeshProUGUI currentTileCostText;
   
   [Space(20)] [Title("ICON SETTINGS")]
   [SerializeField] private Image orthographicButtonImage;
   [SerializeField] private List<Sprite> orthographicButtonSprites;
   [SerializeField] private Image clickModeButtonImage;
   [SerializeField] private List<Sprite> clickModeButtonSprites;

   private CanvasGroup _dockCanvasGroup;

   private bool _dockOpened = true;
   private bool _subDockOpened;
   private bool _reopenSubDock;

   public static Action OnUIClick;
   
   private void Awake()
   {
      Setup();
   }

   private void Update()
   {
      if (Input.GetKeyDown(KeyCode.R))
      {
         ToggleDock(); 
      }

      if (Input.GetKeyDown(KeyCode.E))
      {
         ToggleSubDock();
      }
   }

   private void Setup()
   {
      _dockCanvasGroup = dockObject.GetComponent<CanvasGroup>();
      
      CameraController.OnOrthographicSwitch += SwitchOrthographicButtonSprite;
      MouseController.OnStartPointSet += UpdateStartPointText;
      MouseController.OnEndPointSet += UpdateEndPointText;
      MouseController.OnCurrentTileChange += UpdateCurrentTileInfo;
      MouseController.OnCurrentTileClear += ClearTileInfo;
      MouseController.OnClearPoints += ClearPointsText;
      PathFinding.OnPathFound += UpdateDistance;
      
      ToggleSubDock();
   }
   
   public void ToggleDock()
   {
      InformUIClicked();
      
      switch (_dockOpened)
      {
         case true:
            _dockCanvasGroup.interactable = false;

            if (_subDockOpened)
            {
               ToggleSubDock(true); 
            }
            
            closeDockButton.SetActive(false);
            
            dockObject.DOMove(dockClosedAnchor.position, settingsFile.dockAnimationDuration).SetEase(Ease.InSine).OnComplete(
               () =>
               {
                  openDockButton.SetActive(true);
               });
            break;
         case false:

            openDockButton.SetActive(false);
            
            dockObject.DOMove(dockOpenAnchor.position, settingsFile.dockAnimationDuration).SetEase(Ease.OutSine).OnComplete(() =>
            {
               closeDockButton.SetActive(true);
               
               if (_reopenSubDock)
               {
                  ToggleSubDock(); 
               }
               _dockCanvasGroup.interactable = true;
            });
            break;
      }

      _dockOpened = !_dockOpened;
   }

   public void ToggleSubDock()
   {
      if (!_dockOpened) return;
    
      InformUIClicked();
      
      switch (_subDockOpened)
      {
         case true:
            subDockObject.DOLocalMove(subDockClosedAnchor.localPosition, settingsFile.subDockAnimationDuration).SetEase(Ease.InSine).OnComplete(
               () =>
               {
                  closeDockButton.SetActive(true);
                  subDockObject.gameObject.SetActive(false);
               });
            break;
         case false:
            
            closeDockButton.SetActive(false);
            subDockObject.gameObject.SetActive(true);
            
            subDockObject.DOLocalMove(subDockOpenAnchor.localPosition, settingsFile.subDockAnimationDuration).SetEase(Ease.OutSine).OnComplete(
               () =>
               {
                  closeSubDockButton.SetActive(true);
               });
            break;
      }

      _subDockOpened = !_subDockOpened;
   }

   public void ToggleClickMode()
   {
      InformUIClicked();
      
      if (clickModeButtonImage.sprite == clickModeButtonSprites[0])
      {
         clickModeButtonImage.sprite = clickModeButtonSprites[1];
         settingsFile.obstaclePainting = true;
      }
      else
      {

         clickModeButtonImage.sprite = clickModeButtonSprites[0];
         settingsFile.obstaclePainting = false;
      }
   }

   public void ToggleHelpPanel()
   {
      InformUIClicked();
      
      helpButton.SetActive(!helpButton.activeSelf);
      
      if (helpPanel.gameObject.activeSelf)
      {
         helpPanel.interactable = false;
         helpPanel.blocksRaycasts = false;
         helpPanel.DOFade(0, 0.2f).OnComplete(() =>
         {
            helpPanel.gameObject.SetActive(false);
         });
      }
      else
      {
         helpPanel.gameObject.SetActive(true);
         helpPanel.blocksRaycasts = true;
         helpPanel.DOFade(1, 0.2f).OnComplete(() =>
         {
            helpPanel.interactable = true;
         });
      }
   }
   
   private void ClearPointsText()
   {
      startPointCoordinatesText.text = "[0 , 0]";
      endPointCoordinatesText.text = "[0 , 0]";
      distanceText.text = "00 TILES";
   }
   
   private void UpdateStartPointText(HexTile startTile)
   {
      startTile.GetTileInfo(out Vector2 tileCoordinates);
      startPointCoordinatesText.text = $"[{tileCoordinates.x} , {tileCoordinates.y}]";
   }
   
   private void UpdateEndPointText(HexTile endTile)
   {
      endTile.GetTileInfo(out Vector2 tileCoordinates);
      endPointCoordinatesText.text = $"[{tileCoordinates.x} , {tileCoordinates.y}]";
   }

   private void UpdateDistance(int distance)
   {
      distanceText.text = distance + " TILES";
   }

   private void UpdateCurrentTileInfo(HexTile currentTile)
   {
      currentTile.GetTileInfo(out Vector2 tileCoordinates);
         
      currentTileCoordinatesText.text = $"[{tileCoordinates.x} , {tileCoordinates.y}]";
      
      if (currentTile.GCost >= 99999999)
      {
         currentTileCostText.text = "G:00 | H:00 | F: 00";
      }
      else
      {
         currentTileCostText.text = $"G:{currentTile.GCost} | H:{currentTile.HCost} | F: {currentTile.FCost}";
      }
   }

   private void ClearTileInfo()
   {
      currentTileCoordinatesText.text = "[0 , 0]";
      currentTileCostText.text = "G:00 | H:00 | F: 00";
   }
   
   private void ToggleSubDock(bool saveState)
   {
      if (!_dockOpened) return;
      
      switch (_subDockOpened)
      {
         case true:
            subDockObject.DOLocalMove(subDockClosedAnchor.localPosition, settingsFile.subDockAnimationDuration).SetEase(Ease.InSine);
            break;
         case false:
            subDockObject.DOLocalMove(subDockOpenAnchor.localPosition, settingsFile.subDockAnimationDuration).SetEase(Ease.OutSine).OnComplete(
               () =>
               {
                  closeSubDockButton.SetActive(true);
               });;
            break;
      }

      _reopenSubDock = true;
      _subDockOpened = !_subDockOpened;
   }

   private void SwitchOrthographicButtonSprite()
   {
      switch (orthographicButtonImage.sprite == orthographicButtonSprites[0])
      {
         case true:
            orthographicButtonImage.sprite = orthographicButtonSprites[1];
            break;
         case false:
            orthographicButtonImage.sprite = orthographicButtonSprites[0];
            break;
      }
   }

   private void InformUIClicked()
   {
      OnUIClick?.Invoke();
   }
   
   private void OnDisable()
   {
      CameraController.OnOrthographicSwitch -= SwitchOrthographicButtonSprite;
      MouseController.OnStartPointSet -= UpdateStartPointText;
      MouseController.OnEndPointSet -= UpdateEndPointText;
      MouseController.OnCurrentTileChange -= UpdateCurrentTileInfo;
      MouseController.OnCurrentTileClear -= ClearTileInfo;
      MouseController.OnClearPoints -= ClearPointsText;
      PathFinding.OnPathFound -= UpdateDistance;
   }
}

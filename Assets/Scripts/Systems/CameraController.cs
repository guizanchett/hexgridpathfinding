using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CameraController : MonoBehaviour
{
   [SerializeField] private ScriptableSettings settingsFile;
   [SerializeField] private List<Transform> cameraAnglesAnchorList;
   [SerializeField] private Transform orthographicAnchor;

   private Camera _camera;

   private Transform _cameraTransform;

   private int _currentCameraIndex;
   
   public static Action OnOrthographicSwitch;
   
   private void Awake()
   {
      _camera = Camera.main;
      _camera.orthographicSize = 16f;
      _cameraTransform = _camera.transform;
      _cameraTransform.SetPositionAndRotation(cameraAnglesAnchorList[0].position, cameraAnglesAnchorList[0].rotation);
   }

   public void SwitchCameraView()
   {
      if (_camera.orthographic)
      {
         _camera.orthographic = false;
         OnOrthographicSwitch?.Invoke();
      }
      
      _currentCameraIndex++;

      if (_currentCameraIndex >= cameraAnglesAnchorList.Count)
      {
         _currentCameraIndex = 0;
      }

      _cameraTransform.DOMove(cameraAnglesAnchorList[_currentCameraIndex].position,
         settingsFile.viewTransitionDuration);
      _cameraTransform.DORotateQuaternion(cameraAnglesAnchorList[_currentCameraIndex].rotation,
         settingsFile.viewTransitionDuration);
   }

   public void SwitchOrthographic()
   {

      OnOrthographicSwitch?.Invoke();
      
      switch (_camera.orthographic)
      {
         case true:
            _camera.orthographic = false;
            
            _cameraTransform.DOMove(cameraAnglesAnchorList[_currentCameraIndex].position,
               settingsFile.viewTransitionDuration);
            _cameraTransform.DORotateQuaternion(cameraAnglesAnchorList[_currentCameraIndex].rotation,
               settingsFile.viewTransitionDuration);
            break;
         case false:
            _cameraTransform.DOMove(orthographicAnchor.position,
               settingsFile.viewTransitionDuration);
            _cameraTransform.DORotateQuaternion(orthographicAnchor.rotation,
               settingsFile.viewTransitionDuration).OnComplete(() =>
            {
               _camera.orthographic = true;
            });
            break;
      }
   }
}

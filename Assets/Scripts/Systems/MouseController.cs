using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseController : MonoBehaviour
{
   [SerializeField] private ScriptableSettings settings;
   [SerializeField] private PathFinding PathFinding;
   [SerializeField] private HexGrid gridObject;
   [SerializeField] private GameObject startPinObject;
   [SerializeField] private GameObject endFlagObject;
   [SerializeField] private LayerMask layerToHit;
   [SerializeField] private LayerMask uiLayerMask;
   
   private Vector3 screenPosition;
  

   private HexTile startTile;
   private HexTile endTile;
   
   private HexTile currentTile;
   private HexTile previousTile;

   public static Action<HexTile> OnStartPointSet;
   public static Action<HexTile> OnEndPointSet;
   public static Action<HexTile> OnCurrentTileChange;
   public static Action OnObstaclePlaced;
   public static Action OnCurrentTileClear;
   public static Action OnClearPoints;

   private void Update()
   {
      RaycastControl();

      MouseControl();
   }

   private void RaycastControl()
   {
      screenPosition = Input.mousePosition;

      Ray ray = Camera.main.ScreenPointToRay(screenPosition);

      HoverNewTile(ray);
   }

   private void MouseControl()
   {
      if (Input.GetMouseButtonDown(0))
      {
         if (currentTile != null)
         {
            if (settings.obstaclePainting)
            {
               PaintObstacle();
            }
            else
            {
               SetStartPoint();
            }
         }
      }

      if (Input.GetMouseButtonDown(1))
      {
         SetEndPoint();
      }
   }
   
   private void HoverNewTile(Ray ray)
   {
      if (EventSystem.current.IsPointerOverGameObject()) return;
      
      if (Physics.Raycast(ray, out RaycastHit hit, 1000, layerToHit))
      {
         var newTile = hit.transform.GetComponentInParent<HexTile>();

         if (newTile != currentTile)
         {
            if (previousTile != null)
            {
               if (previousTile != currentTile)
               {
                  previousTile = currentTile;
                  previousTile.ExitHoverAnimation();
               }
            }
            else
            {
               if (currentTile != null)
               {
                  previousTile = currentTile;
                  previousTile.ExitHoverAnimation(); 
               }
            }
         
            currentTile = newTile;
            OnCurrentTileChange?.Invoke(currentTile);
            currentTile.HoverAnimation();
         }
      }
      else
      {
         if (currentTile != null)
         {
            if (currentTile != startTile && currentTile != endTile && !currentTile.IsPath)
            {
               if (currentTile.TileType == TileType.Walkable)
               {
                  currentTile.SetAsWalkable();
               }
            }
            
            currentTile = null;
         }

         if (previousTile != null)
         {
            if (previousTile != startTile && previousTile != endTile && !previousTile.IsPath)
            {
               if (previousTile.TileType == TileType.Walkable)
               {
                  previousTile.SetAsWalkable();
               }
            }
            
            previousTile = null;
         }
         
         OnCurrentTileClear?.Invoke();
         
         if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
         {
            if (startTile != null)
            {
               if (startTile.TileType == TileType.Walkable)
               {
                  startTile.SetAsWalkable();
               }
            }
            
            startTile = null;
            startPinObject.SetActive(false);


            if (endTile != null)
            {
               if (endTile.TileType == TileType.Walkable)
               {
                  endTile.SetAsWalkable();
               }
            }
            endTile = null;
            endFlagObject.SetActive(false);

            
            if (PathFinding.GetLastPath() != null && PathFinding.GetLastPath().Count > 0)
            {
               foreach (var tile in PathFinding.GetLastPath())
               {
                  if (tile.TileType == TileType.Walkable)
                  {
                     ICell hexTile = tile;
                     hexTile.SetAsWalkable();
                  }
               }
               
               OnClearPoints?.Invoke();
               PathFinding.ClearLastPath();
            }
         }
      }
   }

   private void SetStartPoint()
   {
      if (currentTile.TileType != TileType.Walkable) return;

      if (startTile != null)
      {
         startTile.SetAsWalkable();
      }

      startTile = currentTile;
      startTile.SetAsPath();
      
      OnStartPointSet?.Invoke(startTile);
      
      currentTile.GetTileInfo(out Vector3 tileCenterPos);
      startPinObject.SetActive(true);
      startPinObject.transform.position =
         tileCenterPos + new Vector3(0, ScriptableSettings.REF_PLACEABLES_Y_OFFSET, 0);
      
      CheckForWaypoints();
   }

   private void SetEndPoint()
   {
      if (currentTile != null)
      {
         if (currentTile.TileType != TileType.Walkable) return;

         if (endTile != null)
         {
            endTile.SetAsWalkable();
         }
         
         endTile = currentTile;
         endTile.SetAsPath();

         OnEndPointSet?.Invoke(endTile);
         
         endFlagObject.SetActive(true);
         endTile.GetTileInfo(out Vector3 endTileCenterPos);
         endFlagObject.transform.position =
            endTileCenterPos + new Vector3(0, ScriptableSettings.REF_PLACEABLES_Y_OFFSET, 0);
         
         CheckForWaypoints();
      }
   }

   private void CheckForWaypoints()
   {
      if (startTile != null & endTile != null)
      {
         if (endTile != startTile)
         {
            PathFinding.FindPath(startTile, endTile, gridObject);
         }
      }
   }
   
   private void PaintObstacle()
   {
      if (currentTile.TileType == TileType.Obstacle)
      {
         currentTile.SetAsWalkable();
      }
      else
      {
         OnObstaclePlaced?.Invoke();
         currentTile.SetAsObstacle();
      }
   }
}

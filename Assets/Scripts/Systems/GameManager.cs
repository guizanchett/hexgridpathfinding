using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private ScriptableSettings settingsFile;

    public static Action<ScriptableSettings> OnSendSettings;

    private void Awake()
    {
        Setup();
    }

    public void Setup()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Additive);
        Debug.unityLogger.logEnabled = !settingsFile.disableLogs;
        HexGrid.OnGridCreated += SendSettings;
    }

    public void SendSettings()
    {
        OnSendSettings?.Invoke(settingsFile);
    }
}

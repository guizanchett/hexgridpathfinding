using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private ScriptableSettings settingsFile;

    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();

        UIManager.OnUIClick += PlayUISound;
        MouseController.OnCurrentTileChange += PlayHoverSound;
        MouseController.OnStartPointSet += PlayPointPlacementSound;
        MouseController.OnEndPointSet += PlayPointPlacementSound;
        MouseController.OnObstaclePlaced += PlayObstaclePlacementSound;
    }

    private void PlayUISound()
    {
        _audioSource.PlayOneShot(settingsFile.uiClick, 0.25f);
    }

    private void PlayHoverSound(HexTile tile)
    {
        if (tile.TileType == TileType.Obstacle) return;
        _audioSource.PlayOneShot(settingsFile.hoverSound,0.02f);
    }
    
    private void PlayObstaclePlacementSound()
    {
        _audioSource.PlayOneShot(settingsFile.placeObstacleSound,0.1f);
    }

    private void PlayPointPlacementSound(HexTile tile)
    {
        _audioSource.PlayOneShot(settingsFile.placePointSound, 0.1f);
    }
 
    private void OnDisable()
    {
        UIManager.OnUIClick -= PlayUISound;
        MouseController.OnCurrentTileChange -= PlayHoverSound;
        MouseController.OnStartPointSet -= PlayPointPlacementSound;
        MouseController.OnEndPointSet -= PlayPointPlacementSound;
        MouseController.OnObstaclePlaced -= PlayObstaclePlacementSound;
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class HexGrid : MonoBehaviour, IMap
{
   [field: SerializeField] public HexOrientation Orientation { get; set; }
   [field:SerializeField] public int Width { get; set; }
   [field:SerializeField] public int Height { get; set; }
   [field:SerializeField] public float HexSize { get; set; }
   [field:SerializeField] public GameObject HexPrefab { get; set; }

   [SerializeField] private ScriptableSettings settingsFile;
   [SerializeField] private Vector3 prefabPosOffset;
   [SerializeField] private Vector3 prefabCustomRotation;

   private List<HexTile> _gridTiles;

   public static Action OnGridCreated;
   
   private void Start()
   {
      Spawn3DGrid();
   }

   public void Spawn3DGrid()
   {
      _gridTiles = new List<HexTile>();
      
      for (int z = 0; z < Height; z++)
      {
         for (int x = 0; x < Width; x++)
         {
            Vector3 centerPosition = HexMetrics.Center(HexSize, x, z, Orientation) + transform.position;

            Vector3 cellPosition = centerPosition + prefabPosOffset;
            
            GameObject gridCell = Instantiate(HexPrefab, cellPosition, Quaternion.Euler(prefabCustomRotation));
            
            float duration = Random.Range(0, 1.5f);
            
            HexTile tileComponent = gridCell.GetComponent<HexTile>();
            
            tileComponent.SetHexTile(centerPosition, new Vector2(z,x), HexSize);
            
            if (settingsFile.randomizeObstacles)
            {
               float randomNumber = Random.Range(0f, 1f);
               
               if (randomNumber <= settingsFile.randomObstacleChance)
               {
                  tileComponent.SetAsObstacle();
               }
            }

            gridCell.name = $"HEX TILE [{z}, {x}]";
            gridCell.transform.SetParent(transform);
            
            tileComponent.SetGridReference(this);

            _gridTiles.Add(tileComponent);
         }
      }
      
      OnGridCreated?.Invoke();
      GetGridObject(3,4);
   }

   public HexTile GetGridObject(int xCoordinate, int yCoordinate)
   {
      Vector2 coordinatesToCheck = new Vector2(xCoordinate, yCoordinate);

      foreach (var tile in _gridTiles)
      {
         Vector2 tileCoordinates = new Vector2();
         
         tile.GetTileInfo(out tileCoordinates);

         if (tileCoordinates == coordinatesToCheck)
         {
            return tile;
         }
      }

      return null;
   }
   
   public List<HexTile> GetGridTileSet()
   {
      return _gridTiles;
   }

   private void OnDrawGizmos()
   {
      if (!settingsFile.showGizmos) return;
      
      for (int z = 0; z < Height; z++)
      {
         for (int x = 0; x < Width; x++)
         {
            Vector3 centerPosition = HexMetrics.Center(HexSize, x, z, Orientation) + transform.position;

            for (int s = 0; s < HexMetrics.Corners(HexSize, Orientation).Length; s++)
            {
               Gizmos.DrawLine(centerPosition + HexMetrics.Corners(HexSize,Orientation)[s % 6],
                  centerPosition + HexMetrics.Corners(HexSize, Orientation)[(s + 1) % 6]);
            }
         }
      }
   }
}

public enum HexOrientation
{
   FlatTop,
   PointyTop
}
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

public class HexTile : MonoBehaviour, ICell
{
    [SerializeField] private Transform mesh;
    [SerializeField] private TextMeshProUGUI indexText;

    public int GCost { get; set; }
    public int HCost { get; set; }
    public int FCost { get; set; }
    public TileType TileType { get; set; }
    
    public Vector3 CenterPosition { get; set; }
    
    public Vector2 Index { get; set; }
    
    public List<HexTile> NeighboursList { get; set; }
    
    public bool IsPath { get; set; }
    
    private Material _startingMaterial;
    
    private MeshRenderer _meshRenderer;
    
    private ScriptableSettings _settingsFile;

    private HexGrid _grid;
    
    private Vector3 _originalMeshPos;
    
    
    private bool _hoverOn;
    private bool _blockHover = true;
    
    public ICell PreviousTile { get; set; }

    public Tween hoverUpTween;
    public Tween hoverDownTween;
    
    private void Awake()
    {
        GameManager.OnSendSettings += SetSettingsFile;
        _meshRenderer = GetComponentInChildren<MeshRenderer>();
        
        if (_meshRenderer)
        {
            _startingMaterial = _meshRenderer.material;
        }
        
        HexGrid.OnGridCreated += FindNeighbours;
        HexGrid.OnGridCreated += TileInPlace;
        PathFinding.OnPathCleared += ResetCosts;
    }

    public void SetHexTile(Vector3 centerPos, Vector2 index, float hexSize)
    {
        if (hexSize != ScriptableSettings.REF_HEXSIZE)
        {
            float newHexSize = hexSize;
            float scaleRatio = newHexSize / ScriptableSettings.REF_HEXSIZE;
            Vector3 newScale = new Vector3(ScriptableSettings.REF_HEXMESHSIZE,ScriptableSettings.REF_HEXMESHSIZE,ScriptableSettings.REF_HEXMESHSIZE) * scaleRatio;

            mesh.localScale = newScale;
        }

        CenterPosition = centerPos;
        Index = index;
    }

    public void GetTileInfo(out Vector3 centerPos, out Vector2 index)
    {
        centerPos = new Vector3();
        index = new Vector2();
        
        centerPos = CenterPosition;
        index = Index;
    }

    public void GetTileInfo(out Vector3 centerPos)
    {
        centerPos = new Vector3();
        centerPos = CenterPosition;
    }
    
    public void GetTileInfo(out Vector2 index)
    {
        index = new Vector2();
        
        index = Index;
    }
    
    public void HoverAnimation()
    {
        if (_hoverOn || TileType == TileType.Obstacle || _blockHover) return;

        _hoverOn = true;
        SetHoverColor(); 
        DOTween.Kill(hoverDownTween);
        hoverUpTween = mesh.DOLocalMoveY(_originalMeshPos.y + _settingsFile.hexHoverHeight, _settingsFile.hexHoverDuration);
    }

    public void ExitHoverAnimation()
    {
        if (TileType == TileType.Obstacle || _blockHover) return;
        
        _hoverOn = false;
        
        SetNormalColor();
        DOTween.Kill(hoverUpTween);
        hoverDownTween = mesh.DOLocalMoveY(_originalMeshPos.y, _settingsFile.hexHoverDuration);
    }

    public void SetBlockHover(bool newState)
    {
        _blockHover = newState;

        if (_blockHover && TileType != TileType.Obstacle)
        {
            hoverDownTween = mesh.DOLocalMoveY(_originalMeshPos.y, _settingsFile.hexHoverDuration);

            if (!IsPath)
            {
                SetNormalColor();
            }
        }
    }

    public void ResetCosts()
    {
        GCost = int.MaxValue;
        HCost = 0;
        FCost = 0;
    }
    
    public void GetFCost()
    {
        FCost = GCost + HCost;
    }

    public void SetGridReference(HexGrid grid)
    {
        _grid = grid;
    }
    
    public void SetAsPath()
    {
        _blockHover = true;
        IsPath = true;
        mesh.DOLocalMoveY(_originalMeshPos.y, _settingsFile.hexHoverDuration);
        _meshRenderer.material.SetFloat("_PathColorIntensity", 0.25f);
        _meshRenderer.material.SetFloat("_ObstacleColorIntensity", 1f);
       
        SetNormalColor();
    }

    public void SetAsWalkable()
    {
        _blockHover = false;
        IsPath = false;
        _hoverOn = false;
        TileType = TileType.Walkable;
        DOTween.Kill(hoverUpTween);
        DOTween.Kill(hoverDownTween);
        hoverDownTween = mesh.DOLocalMoveY(_originalMeshPos.y, _settingsFile.hexHoverDuration);
        _meshRenderer.material.SetFloat("_PathColorIntensity", 1f);
        _meshRenderer.material.SetFloat("_ObstacleColorIntensity", 1f);
        
        SetNormalColor();
    }
    
    public List<HexTile> GetNeighboursList()
    {
        return NeighboursList;
    }

    public void SetAsObstacle()
    {
        _blockHover = true;
        TileType = TileType.Obstacle;
        DOTween.Kill(hoverUpTween);
        DOTween.Kill(hoverDownTween, false);
        hoverDownTween = mesh.transform.DOLocalMoveY(0.1f, 0.35f);
        _meshRenderer.material.SetFloat("_PathColorIntensity", 1f);
        _meshRenderer.material.SetFloat("_ObstacleColorIntensity", 0.25f);
        
        SetNormalColor();
    }
    
    private void SetHoverColor()
    {
        if (_settingsFile.obstaclePainting)
        {
            _meshRenderer.material.SetFloat("_PlaceObstacleColorIntensity", 0f);
        }
        else
        {
            _meshRenderer.material.SetFloat("_HoverColorIntensity", 0f); 
        }
        
    }

    private void SetNormalColor()
    {
        _meshRenderer.material.SetFloat("_HoverColorIntensity", 1f);
        _meshRenderer.material.SetFloat("_PlaceObstacleColorIntensity", 1f);
    }
    
    private void SetSettingsFile(ScriptableSettings file)
    {
        _settingsFile = file;
    }

    private void TileInPlace()
    {
        _blockHover = TileType != TileType.Walkable;

        _originalMeshPos = mesh.transform.localPosition;
    }
    
    private void FindNeighbours()
    {
        NeighboursList = new List<HexTile>();

        bool oddRow = Index.y % 2 == 1; 
        
        
        if (Index.x - 1 >= 0) //LEFT NEIGHBOUR
        {
            NeighboursList.Add(_grid.GetGridObject((int)Index.x - 1, (int)Index.y));
        }

        if (Index.x + 1 < _grid.Width) // RIGHT NEIGHBOUR
        {
            NeighboursList.Add(_grid.GetGridObject((int)Index.x + 1, (int) Index.y));
        }

        if (Index.y - 1 >= 0) //DOWN NEIGHBOUR
        {
            NeighboursList.Add(_grid.GetGridObject((int)Index.x, (int)Index.y -1));
        }

        if (Index.y + 1 < _grid.Height) //UP NEIGHBOUR
        {
            NeighboursList.Add(_grid.GetGridObject((int)Index.x, (int)Index.y + 1));
        }
        
        if (oddRow)
        {
            if (Index.y + 1 < _grid.Height && Index.x + 1 < _grid.Width)
            {
                NeighboursList.Add(_grid.GetGridObject((int)Index.x + 1, (int)Index.y+1));
            }

            if (Index.y - 1 >= 0 && Index.x + 1 < _grid.Width)
            {
                NeighboursList.Add(_grid.GetGridObject((int)Index.x + 1, (int)Index.y -1));
            }
        }
        else
        {
            if (Index.y + 1 < _grid.Height && Index.x - 1 >= 0)
            {
                NeighboursList.Add(_grid.GetGridObject((int)Index.x - 1, (int)Index.y + 1));
            }

            if (Index.y - 1 >= 0 && Index.x - 1 >= 0)
            {
                NeighboursList.Add(_grid.GetGridObject((int)Index.x -1,(int)Index.y -1));
            }
        }
    }

    private void OnDisable()
    {
        GameManager.OnSendSettings -= SetSettingsFile;
        HexGrid.OnGridCreated -= FindNeighbours;
        HexGrid.OnGridCreated -= TileInPlace;
        PathFinding.OnPathCleared -= ResetCosts;
    }
}

public enum TileType
{
    Walkable,
    Obstacle,
    Resource
}
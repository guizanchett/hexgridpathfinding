using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMap
{
    public HexOrientation Orientation { get; set; }
    public int Width { get; set; }
    public int Height { get; set; }
    public float HexSize { get; set; }
    public GameObject HexPrefab { get; set; }

    public void Spawn3DGrid();

    public HexTile GetGridObject(int xCoordinate, int yCoordinate);
    
    
}

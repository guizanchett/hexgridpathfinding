using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICell 
{
    public int GCost { get; set; }
    public int HCost { get; set; }
    public int FCost { get; set; }
    
    public TileType TileType { get; set; }
    
    public Vector3 CenterPosition { get; set; }
    
    public Vector2 Index { get; set; }
    
    public List<HexTile> NeighboursList { get; set; }
    
    public bool IsPath { get; set; }

    public void SetAsWalkable();

    public void GetFCost();
    
    public void SetAsPath();

    public void SetAsObstacle();

    public ICell PreviousTile { get; set; }
    
    public List<HexTile> GetNeighboursList();
}

using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu (fileName = "ScriptableSettings", menuName = "Create Settings File", order = 0)]
public class ScriptableSettings : ScriptableObject
{
   [Title("GENERAL SETTINGS")]
   public bool showGizmos;
   public bool randomizeObstacles;
   public bool disableLogs;
   public bool obstaclePainting;
   
   public float randomObstacleChance;
   public float hexHoverDuration;
   public float hexHoverHeight;

   [Space(30)] [Title("UI SETTINGS")]
   public float dockAnimationDuration;
   public float subDockAnimationDuration;

   [Space(30)] [Title("CAMERA SETTINGS")]
   public float viewTransitionDuration;

   [Space(30)] [Title("AUDIO SETTINGS")] 
   public AudioClip uiClick;
   public AudioClip hoverSound;
   public AudioClip placePointSound;
   public AudioClip placeObstacleSound;
   
   public const int STRAIGHT_MOVEMENT_COST = 10;
   public const int DIAGONAL_MOVEMENT_COST = 14; //USE ONLY FOR SQUARE GRID. USING STRAIGHT VALUE ONLY FOR HEXAGONAL.
   public const float REF_HEXSIZE = 0.85f;
   public const float REF_HEXMESHSIZE = 150;
   public const float REF_PLACEABLES_Y_OFFSET = 0.25f;
}

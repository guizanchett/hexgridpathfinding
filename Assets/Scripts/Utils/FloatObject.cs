using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class FloatObject : MonoBehaviour
{
   [SerializeField] private float amount;
   [SerializeField] private float floatDuration;

   private void Awake()
   {
       transform.DOMoveY(amount, floatDuration).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
   }
}

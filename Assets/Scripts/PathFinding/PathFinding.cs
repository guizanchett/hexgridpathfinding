using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinding : MonoBehaviour, IPathFinder
{
    [field: SerializeField]public HexGrid HexGrid { get; set; }

    private ScriptableSettings _settingsFile;
    
    private List<ICell> _openList;
    private List<ICell> _closedList;
    private List<ICell> _path;

    public static Action<int> OnPathFound;
    public static Action OnPathCleared;
    
    private void Awake()
    {
        GameManager.OnSendSettings += GetSettingsFile;
    }

    public List<ICell> GetLastPath()
    {
        return _path;
    }
    

    public void ClearLastPath()
    {
        if (_path != null)
        {
            foreach (var tile in _path)
            {
                if (tile.TileType == TileType.Walkable)
                {
                    tile.SetAsWalkable(); 
                }
            }
        }
        
        OnPathCleared?.Invoke();
        _path = new List<ICell>();
    }

    public List<ICell> FindPath(ICell _startTile, ICell _endTile, HexGrid grid)
    {
        ClearLastPath();
        
        ICell startTile = _startTile;
        ICell endTile = _endTile;
                
        _openList = new List<ICell>{startTile};
        _closedList = new List<ICell>();

        for (int y = 0; y < HexGrid.Height; y++)
        {
            for (int x = 0; x < HexGrid.Width; x++)
            {
                HexTile tile = HexGrid.GetGridObject(x, y);
                tile.GCost = int.MaxValue;
                tile.GetFCost();
                tile.PreviousTile = null;
            }
        }

        startTile.GCost = 0;
        startTile.HCost = CalculateDistance(startTile, endTile);
        startTile.GetFCost();

        while (_openList.Count > 0)
        {
            ICell currentTile = GetLowestFCost(_openList);

            if (currentTile == endTile)
            {
                _path = new List<ICell>();
                _path = CalculatePath(endTile);
                return (_path);
            }

            _openList.Remove(currentTile);
            _closedList.Add(currentTile);

            foreach (HexTile neighbour in currentTile.GetNeighboursList())
            {
                if (_closedList.Contains(neighbour)) continue;

                if (neighbour.TileType == TileType.Obstacle)
                {
                    _closedList.Add(neighbour);
                    continue;
                }

                int possibleLowerGCost = currentTile.GCost + CalculateDistance(currentTile, neighbour);

                if (possibleLowerGCost < neighbour.GCost)
                {
                    neighbour.PreviousTile = currentTile;
                    neighbour.GCost = possibleLowerGCost;
                    neighbour.HCost = CalculateDistance(neighbour, endTile);
                    neighbour.GetFCost();

                    if (!_openList.Contains(neighbour))
                    {
                        _openList.Add(neighbour);
                    } 
                }
            }
        }

        return null;
    }

    public List<ICell> CalculatePath(ICell endTile)
    {
        List<ICell> path = new List<ICell>();
        path.Add(endTile);

        ICell currentTile = endTile;
        
        while (currentTile.PreviousTile != null)
        {
            path.Add(currentTile.PreviousTile);
            currentTile = currentTile.PreviousTile;
        }

        foreach (var tile in path)
        {
            HexTile hexTile = (HexTile)tile;
            hexTile.SetAsPath();
        }
        
        path.Reverse();
        
        OnPathFound?.Invoke(path.Count);
        
        return path;
    }

    public int CalculateDistance(ICell tileA, ICell tileB)
    {
        return Mathf.RoundToInt(ScriptableSettings.STRAIGHT_MOVEMENT_COST * Vector3.Distance(tileA.CenterPosition, tileB.CenterPosition));
    }

    public ICell GetLowestFCost(List<ICell> tilesList)
    {
        ICell lowestFCostTile = tilesList[0];

        for (int i = 0; i < tilesList.Count; i++)
        {
            if (tilesList[i].FCost < lowestFCostTile.FCost)
            {
                lowestFCostTile = tilesList[i];
            }
        }

        return lowestFCostTile;
    }
    
    private void GetSettingsFile(ScriptableSettings settings)
    {
        _settingsFile = settings;
    }

    private void OnDisable()
    {
        GameManager.OnSendSettings -= GetSettingsFile;
    }
}

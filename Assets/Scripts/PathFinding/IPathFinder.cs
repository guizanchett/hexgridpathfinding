using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPathFinder
{
    public HexGrid HexGrid { get; set; }
    
    public static Action<int> OnPathFound;
    public static Action OnPathCleared;

    public List<ICell> FindPath(ICell _startTile, ICell _endTile, HexGrid grid);

    public void ClearLastPath();

    public List<ICell> CalculatePath(ICell endTile);

    public int CalculateDistance(ICell tileA, ICell tileB);

    public ICell GetLowestFCost(List<ICell> tilesList);

    public List<ICell> GetLastPath();
}
